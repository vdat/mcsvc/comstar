import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing/landing.component';
import {RouterModule, Routes} from '@angular/router';
import { PagesComponent } from './pages.component';
import { LoginComponent } from './login/login.component';

export const PagesRoutes: Routes = [
  { path: 'landing', component: LandingComponent},
  { path: 'login', component: LoginComponent}
];

@NgModule({
  declarations: [LandingComponent, LoginComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(PagesRoutes)
  ]
})
export class PagesModule { }
