import { Component, OnInit } from '@angular/core';
import * as sjcl from 'sjcl';
import * as _ from 'lodash';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {Location} from '@angular/common';
import { JwtHelperService } from '@auth0/angular-jwt';
import {UserService} from '../../shared/services/user.service';
import {UserModel} from '../../models/user.model';

const helper = new JwtHelperService();

export interface IdToken {
  aud: string;
  azp: string;
  c_hash: string;
  email: string;
  email_verified: boolean;
  exp: number;
  given_name: string;
  iat: number;
  iss: string;
  jti: string;
  locale: string;
  name: string;
  nonce: string;
  picture: string;
  sub: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private fragment: any;
  private decoded: any;

  constructor(
    private httpClient: HttpClient,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private userService: UserService,
    private router: Router,
  ) {
    this.activatedRoute.fragment.subscribe(fragment => {
      console.log(fragment);
      // @ts-ignore
      this.fragment = _.fromPairs(Array.from(new URLSearchParams(fragment)));
      this.decoded = this.fragment && this.fragment.id_token && helper.decodeToken(this.fragment.id_token);
      // this.fragment.
      if (this.decoded) {
        this.userService.currentUser = new UserModel(this.decoded);
        this.router.navigate(['/dashboard']);
        console.log(this.decoded);
        if (fragment) {
          this.location.replaceState(window.location.pathname);
        }
      }
    });
  }

  ngOnInit() {
  }

  base64URLEncode(str) {
    return str.toString('base64')
      .replace(/\+/g, '-')
      .replace(/\//g, '_')
      .replace(/=/g, '');
  }

  sha256(buffer) {
    // return crypto.createHash('sha256').update(buffer).digest();
    const bitArray = sjcl.hash.sha256.hash(buffer);
    return sjcl.codec.hex.fromBits(bitArray);
  }

  // requestLogin(configURL: string) {
  //   this.httpClient.get(configURL, { responseType: 'json'}).subscribe((wellKnownOIDCConfiguration: any) => {
  //     const clientId = environment.GOOGLE_OAUTH2_CLIENT_ID;
  //     const authorizationEndpoint = wellKnownOIDCConfiguration.authorization_endpoint;
  //     const redirectURI = window.location.origin + window.location.pathname;
  //     const responseType = 'code id_token';
  //     const nonce = _.times(16, () => _.random(25).toString(36)).join('');
  //     localStorage.setItem('nonce', nonce);
  //
  //     const verifier = this.base64URLEncode(_.times(32, () => _.random(25).toString(36)).join(''));
  //     const challenge = this.base64URLEncode(this.sha256(verifier));
  //     const link = (authorizationEndpoint +
  //       `?audience=&scope=openid profile email&response_type=${responseType}&client_id=${clientId}&code_challenge=${challenge}`
  //       + `&code_challenge_method=S256&redirect_uri=${redirectURI}&nonce=${nonce}`).trim();
  //     console.log(wellKnownOIDCConfiguration);
  //     console.log(link);
  //     window.location.href = link;
  //   });
  // }
  //
  // login(provider: string) {
  //   return this.router.navigate(['/auth/login']);
  //
  //   switch (provider) {
  //     case 'google': {
  //       this.requestLogin('https://accounts.google.com/.well-known/openid-configuration');
  //       break;
  //     }
  //     default: throw new Error('Unknown Provider: ' + provider);
  //   }
  // }
}
