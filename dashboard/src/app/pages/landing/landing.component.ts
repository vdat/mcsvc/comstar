import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor(private readonly location: Location) { }

  ngOnInit() {
    // set browser URL to / for better looking
    this.location.replaceState('/');
  }

}
