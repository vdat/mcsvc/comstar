import {Sms} from '../../resources/pb-web/comstar_pb';

export class SmsModel {
  id: number;
  message: string;
  recipientNumber: string;
  markedForSent = false;
  isProcessing = false;

  constructor(o?: Sms) {
    if (o) {
      this.id = o.getId();
      this.message = o.getBody();
      this.recipientNumber = o.getRecipientNumber();
    }
  }

  toPb(): Sms {
    const sms = new Sms();
    sms.setId(this.id);
    sms.setBody(this.message);
    sms.setRecipientNumber(this.recipientNumber);

    return sms;
  }
}
