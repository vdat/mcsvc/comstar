import {IdToken} from '../pages/login/login.component';


export class UserModel {
  issuer: string;
  email: string;
  emailVerified: boolean;
  givenName: string;
  locale: string;
  name: string;
  picture: string;
  subject: string;

  constructor(o?: any) {
    if (!o) { return; }

    if (o.sub) { // OIDC id_token
      this.issuer = o.iss;
      this.email = o.email;
      this.emailVerified = o.email_verified;
      this.givenName = o.given_name;
      this.locale = o.locale;
      this.name = o.name;
      this.picture = o.identities[0].picture;
      this.subject = o.sub;
    }
  }
}
