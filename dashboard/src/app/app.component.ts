import { Component } from '@angular/core';
import * as _ from 'lodash';
import {UserModel} from './models/user.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {UserService} from './shared/services/user.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {EventService} from './shared/services/event.service';

const jwtHelperService = new JwtHelperService();

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'comstar';
  private decoded: any;
  private fragment: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
    private userService: UserService,
    private eventService: EventService,
  ) {
    this.eventService.events$.subscribe(event => {
      this.eventService.serverEventPublishing(event);
    });
    // this.activatedRoute.fragment.subscribe(fragment => {
    //   console.log(fragment);
    //   // @ts-ignore
    //   this.fragment = _.fromPairs(Array.from(new URLSearchParams(fragment)));
    //   this.decoded = this.fragment && jwtHelperService.decodeToken(this.fragment.access_token);
    //   console.log(this.decoded);
    //   // this.fragment.
    //   if (this.decoded) {
    //     this.userService.currentUser = new UserModel(this.decoded);
    //     // this.router.navigate(['/dashboard']);
    //     if (fragment) {
    //       this.location.replaceState(window.location.pathname);
    //     }
    //   }
    // });
  }
}
