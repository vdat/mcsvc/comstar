import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WrapperComponent} from './shared/layouts/wrapper/wrapper.component';
import {PagesComponent} from './pages/pages.component';
import {AuthGuard} from './shared/guards/auth.guard';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: '',
    component: WrapperComponent,
    // canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: './shared/layouts/wrapper/wrapper.module#WrapperModule'
      }
    ]
  },
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        loadChildren: './pages/pages.module#PagesModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
