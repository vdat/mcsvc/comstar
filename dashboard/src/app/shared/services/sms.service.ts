import {Injectable} from '@angular/core';
import {BehaviorSubject, interval, of} from 'rxjs';
import {concatMap, delay, map} from 'rxjs/operators';
import {SmsModel} from '../../models/sms.model';
import * as ComstarPb from '../../../resources/pb-comstar/comstar_pb';
import {Sms} from '../../../resources/pb-web/comstar_pb';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import { LoremIpsum } from 'lorem-ipsum';
import {EventService} from './event.service';

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 16,
    min: 4
  }
});

@Injectable()
export class SmsService {
  private smsSubject = new BehaviorSubject<SmsModel[]>(null);
  public sms$ = this.smsSubject.asObservable();

  constructor(
    private eventService: EventService,
  ) {
    this.fakeFetch$()
      .pipe(map( sms => {
        const j = [];
        _.each(sms, (job => j.push(new SmsModel(job))));
        console.log(j);
        return j;
      }))
      .subscribe(responses => {
        // console.log(response);
        this.smsSubject.next(responses);
      });

    // SMS
    interval(3000)
      .pipe(
        concatMap(i => of(i).pipe(delay(_.random(1000, 10000)))),
      )
      .subscribe(() => {
        const sms = new Sms();
        sms.setRecipientNumber('0968927457');
        sms.setBody(lorem.generateSentences(1));

        this.eventService.publishingEvent(sms);
      });
  }

  addNewSms$(sms: SmsModel) {
    // this.smsSubject.next(sms.toPb());
    this.eventService.publishingEvent(sms.toPb());
    return of([1]).pipe(delay(_.random(1000, 5000)));
  }

  fakeFetch$() {
    const sms = [];
    for (let i = 0; i < 100; i++) {
      const j = new Sms();
      j.setId(_.random(1000000));
      j.setRecipientNumber('+84' + _.times(9, () => _.random(9).toString(36)).join(''));
      j.setBody(lorem.generateParagraphs(1));

      // console.log(this.randomTimestamp(new Date(2019, 0, 1), new Date()));
      // console.log(this.randomDate(new Date(2019, 0, 1), new Date()).toISOString());
      j.setCreatedAt(this.randomTimestamp(new Date(2019, 0, 1), new Date()));
      // j.setStartTime(this.randomTimestamp(new Date(2019, 0, 1), new Date()));
      // j.setEndTime(this.randomTimestamp(new Date(2019, 0, 1), new Date()));
      // j.setStartTime(this.randomDate(new Date(2019, 0, 1), new Date()));
      // j.setEndTime(this.randomDate(new Date(2019, 0, 1), new Date()));
      sms.push(j);
    }
    return of(sms).pipe(delay(1000));
  }

  randomTimestamp(start, end) {
    const timeMS = this.randomDate(start, end).getTime();

    // Create timestamp
    const timestamp = new google_protobuf_timestamp_pb.Timestamp();
    timestamp.setSeconds(timeMS / 1000);
    timestamp.setNanos((timeMS % 1000) * 1e6);

    // console.log(timestamp);
    return timestamp;
  }


  randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }
}
