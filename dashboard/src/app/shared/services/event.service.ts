import {Injectable, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import * as grpcWeb from 'grpc-web';
import {ComstarClient} from '../../../resources/pb-web/comstar_grpc_web_pb';
import {SubscriptionRequest, Event, Log, Sms} from '../../../resources/pb-web/comstar_pb';
import * as google_protobuf_any_pb from 'google-protobuf/google/protobuf/any_pb';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  private eventSubject = new Subject<Event>();
  public events$ = this.eventSubject.asObservable();

  private comstarClient = new ComstarClient('http://localhost:10000', null, null);
  private serverProjectionSub;

  constructor() {
    console.log('EventService ctor');
    const request = new SubscriptionRequest();
    request.setEventTypesList([Event.Type.LOG]);

    const stream = this.comstarClient.subscription(request, {});

    stream.on('status', (status: grpcWeb.Status) => {
      console.log(status);
    });

    stream.on('data', (event: Event) => {
      if (event.getType() === Event.Type.LOG) {
        const data: google_protobuf_any_pb.Any = event.getData();
        if (data) {
          const log = Log.deserializeBinary(data.getValue_asU8());
          console.log(log.getMessage());
          // this.publishingEvent(log);
        }
      }
    });

    stream.on('error', (error: grpcWeb.Error) => {
      console.log(error);
    });

    stream.on('end', () => {
      console.log('end');
    });
  }

  publishingEvent(o: Event | Sms | Log) {
    let event;

    if (o instanceof Event) {
      event = o;
    } else {
      event = new Event();
      let eventType;
      let bodyType;

      if (o instanceof Sms) {
        eventType = Event.Type.SMS_CREATED;
        bodyType = 'nghinhut.comstar.Sms';
      } else if (o instanceof Log) {
        eventType = Event.Type.LOG;
        bodyType = 'nghinhut.comstar.Log';
      }

      const data = new google_protobuf_any_pb.Any();
      data.pack(o.serializeBinary(), bodyType);
      event.setType(eventType);
      event.setData(data);

      // event.getMetadataMap().set('source', );
      this.addMetadata(event, 'source', '123');
    }

    this.eventSubject.next(event);
  }

  addMetadata(event: Event, key, value: string) {
    const v = new google_protobuf_any_pb.Any();
    v.pack(value);

    event.getMetadataMap().set(key, v);

    return event;
  }

  serverEventPublishing(event: Event) {
    this.comstarClient.publishAnEvent(event, {}, (error, response) => {
      if (error) {
        throw error;
      } else {
        console.log(response);
      }
    });
  }
}
