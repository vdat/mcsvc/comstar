import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {UserModel} from '../../models/user.model';

@Injectable()
export class UserService {
  private userSubject = new BehaviorSubject<UserModel>(null);
  public user$ = this.userSubject.asObservable();
  numbers: string[] = ['Lenovo A7+', '09xxxxxx123', '09xxxxx456', '09xxxxx789'];

  private numbersSubject = new BehaviorSubject<string[]>(null);
  public numbers$ = this.numbersSubject.asObservable();

  private currentNumberSubject = new BehaviorSubject<string>(null);
  public currentNumber$ = this.currentNumberSubject.asObservable();

  constructor() {
    this.numbersSubject.next(this.numbers);
  }

  set currentUser(user: UserModel) {
    this.userSubject.next(user);
  }
  get currentUser(): UserModel {
    return this.userSubject.getValue();
  }
}
