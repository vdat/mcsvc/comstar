import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import {UserService} from '../services/user.service';

@Injectable()
export class AuthGuard implements CanActivateChild {
  constructor(private router: Router,
              private userService: UserService) {
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.userService.currentUser) {
      this.router.navigate(['/auth', 'login'])
        .then(() => {
          // TODO: If Notification (toast) service is present we can show warning message
        });
    } else {
      return true;
    }
  }
}
