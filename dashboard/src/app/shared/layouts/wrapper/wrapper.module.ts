import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WrapperComponent } from './wrapper.component';
import {DashboardComponent} from '../../../dashboard/dashboard.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DashboardModule} from '../../../dashboard/dashboard.module';

export const WrapperRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
];

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    DashboardModule,
    RouterModule.forChild(WrapperRoutes),
    SharedModule,
    NgbModule
  ]
})
export class WrapperModule { }
