import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {WrapperComponent} from './shared/layouts/wrapper/wrapper.component';
import {SharedModule} from './shared/shared.module';
import {WrapperModule} from './shared/layouts/wrapper/wrapper.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PagesComponent} from './pages/pages.component';
import {SmsService} from './shared/services/sms.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {HttpClientModule} from '@angular/common/http';
import {UserService} from './shared/services/user.service';
import {AuthGuard} from './shared/guards/auth.guard';
import {EventService} from './shared/services/event.service';

@NgModule({
  declarations: [
    AppComponent,
    WrapperComponent,
    PagesComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    SharedModule,
    NgbModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  providers: [
    EventService,
    UserService,
    SmsService,
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
