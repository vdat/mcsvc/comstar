import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {Job} from '../../../resources/pb-comstar/comstar_pb';
import {SmsModel} from '../../models/sms.model';
import {SmsService} from '../../shared/services/sms.service';
import {UserService} from '../../shared/services/user.service';

@Component({
  selector: 'app-sms-card',
  templateUrl: './sms-card.component.html',
  styleUrls: ['./sms-card.component.scss']
})
export class SmsCardComponent implements OnInit {
  sms: SmsModel[] = [];
  currentNumber = '';

  constructor(
    public userService: UserService,
    private smsService: SmsService,
  ) {}

  ngOnInit() {
    this.smsService.sms$.subscribe((sms) => {
      if (!sms) { return; }
      console.log(sms);
      this.sms = sms;
    });

    this.userService.numbers$.subscribe(numbers => {
      if (this.currentNumber === '') {
        this.currentNumber = numbers[0];
      }
    });
  }

  addNewSms(id, message, recipientNumber) {
    const sms = new Job();
    sms.setId(id);
    sms.setMessage(message);
    sms.setRecipientNumber(recipientNumber);
    this.sms.push(new SmsModel(sms));
  }

  sendMarkedSms() {
    const markedJobs = this.sms.filter( (sms) => sms.markedForSent && !sms.isProcessing);
    console.log(markedJobs);
    markedJobs.forEach( (sms, index) => {
      this.markJobAsProcessingById(sms.id);
      this.smsService.addNewSms$(sms).subscribe(
        response => {
          this.removeJobById(sms.id);
        },
        error => {

        }
      );
    });
    // this.smss = this.smss.filter(sms => !sms.markedForSent);
  }

  markJobAsProcessingById(id) {
    const idx = this.sms.findIndex( (sms, index) => sms.id === id);
    this.sms[idx].isProcessing = true;
    // this.smss[idx].markedForSent = null;
  }

  removeJobById(id) {
    const idx = this.sms.findIndex( (sms, index) => sms.id === id);
    this.sms.splice(idx, 1);
  }
}

@Pipe({ name: 'countSmsMarkedForSent', pure: false })
export class CountSmsMarkedAsSentPipe implements PipeTransform {
  transform(smss: SmsModel[]) {
    return smss && smss.filter(hero => hero.markedForSent).length || 0;
  }
}
