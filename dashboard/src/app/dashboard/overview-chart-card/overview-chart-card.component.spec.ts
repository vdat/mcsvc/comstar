import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewChartCardComponent } from './overview-chart-card.component';

describe('OverviewChartCardComponent', () => {
  let component: OverviewChartCardComponent;
  let fixture: ComponentFixture<OverviewChartCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverviewChartCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewChartCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
