import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {CountSmsMarkedAsSentPipe, SmsCardComponent} from './sms-card/sms-card.component';
import { OverviewChartCardComponent } from './overview-chart-card/overview-chart-card.component';
import {NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';



@NgModule({
  declarations: [
    DashboardComponent,
    SmsCardComponent,
    OverviewChartCardComponent,
    CountSmsMarkedAsSentPipe
  ],
  imports: [
    CommonModule,
    NgbDropdownModule,
    NgbTooltipModule,
    FormsModule,
    RouterModule
  ],
})
export class DashboardModule { }
