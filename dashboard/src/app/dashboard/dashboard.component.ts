import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UserService} from '../shared/services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import * as _ from 'lodash';
import {UserModel} from '../models/user.model';
import {JwtHelperService} from '@auth0/angular-jwt';
import {EventService} from '../shared/services/event.service';

const jwtHelperService = new JwtHelperService();

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  private decoded: any;
  private fragment: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
    private userService: UserService,
    private eventService: EventService
  ) {
    this.activatedRoute.fragment.subscribe(fragment => {
      console.log(fragment);
      // @ts-ignore
      this.fragment = _.fromPairs(Array.from(new URLSearchParams(fragment)));
      this.decoded = this.fragment && jwtHelperService.decodeToken(this.fragment.access_token);
      console.log(this.decoded);
      // this.fragment.
      if (this.decoded) {
        this.userService.currentUser = new UserModel(this.decoded);
        // this.router.navigate(['/dashboard']);
        if (fragment) {
          this.location.replaceState(window.location.pathname);
        }
      }
    });
  }

  ngOnInit(): void {
    if (this.userService.currentUser === null || this.userService.currentUser === undefined) {
      window.location.replace('/auth/login');
      // alert('not logged-in');
    }
  }

  ngAfterViewInit(): void {

  }

}
