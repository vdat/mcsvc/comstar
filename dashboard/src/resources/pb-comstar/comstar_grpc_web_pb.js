/**
 * @fileoverview gRPC-Web generated client stub for nghinhut.comstar
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


const proto = {};
proto.nghinhut = {};
proto.nghinhut.comstar = require('./comstar_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.nghinhut.comstar.ComstarClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.nghinhut.comstar.ComstarPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.nghinhut.comstar.ListUsersRequest,
 *   !proto.nghinhut.comstar.User>}
 */
const methodDescriptor_Comstar_ListUsersStream = new grpc.web.MethodDescriptor(
  '/nghinhut.comstar.Comstar/ListUsersStream',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.nghinhut.comstar.ListUsersRequest,
  proto.nghinhut.comstar.User,
  /** @param {!proto.nghinhut.comstar.ListUsersRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.User.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.nghinhut.comstar.ListUsersRequest,
 *   !proto.nghinhut.comstar.User>}
 */
const methodInfo_Comstar_ListUsersStream = new grpc.web.AbstractClientBase.MethodInfo(
  proto.nghinhut.comstar.User,
  /** @param {!proto.nghinhut.comstar.ListUsersRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.User.deserializeBinary
);


/**
 * @param {!proto.nghinhut.comstar.ListUsersRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.nghinhut.comstar.User>}
 *     The XHR Node Readable Stream
 */
proto.nghinhut.comstar.ComstarClient.prototype.listUsersStream =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/nghinhut.comstar.Comstar/ListUsersStream',
      request,
      metadata || {},
      methodDescriptor_Comstar_ListUsersStream);
};


/**
 * @param {!proto.nghinhut.comstar.ListUsersRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.nghinhut.comstar.User>}
 *     The XHR Node Readable Stream
 */
proto.nghinhut.comstar.ComstarPromiseClient.prototype.listUsersStream =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/nghinhut.comstar.Comstar/ListUsersStream',
      request,
      metadata || {},
      methodDescriptor_Comstar_ListUsersStream);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.nghinhut.comstar.GetUserByIdRequest,
 *   !proto.nghinhut.comstar.User>}
 */
const methodDescriptor_Comstar_GetUserById = new grpc.web.MethodDescriptor(
  '/nghinhut.comstar.Comstar/GetUserById',
  grpc.web.MethodType.UNARY,
  proto.nghinhut.comstar.GetUserByIdRequest,
  proto.nghinhut.comstar.User,
  /** @param {!proto.nghinhut.comstar.GetUserByIdRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.User.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.nghinhut.comstar.GetUserByIdRequest,
 *   !proto.nghinhut.comstar.User>}
 */
const methodInfo_Comstar_GetUserById = new grpc.web.AbstractClientBase.MethodInfo(
  proto.nghinhut.comstar.User,
  /** @param {!proto.nghinhut.comstar.GetUserByIdRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.User.deserializeBinary
);


/**
 * @param {!proto.nghinhut.comstar.GetUserByIdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.nghinhut.comstar.User)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.nghinhut.comstar.User>|undefined}
 *     The XHR Node Readable Stream
 */
proto.nghinhut.comstar.ComstarClient.prototype.getUserById =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/nghinhut.comstar.Comstar/GetUserById',
      request,
      metadata || {},
      methodDescriptor_Comstar_GetUserById,
      callback);
};


/**
 * @param {!proto.nghinhut.comstar.GetUserByIdRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.nghinhut.comstar.User>}
 *     A native promise that resolves to the response
 */
proto.nghinhut.comstar.ComstarPromiseClient.prototype.getUserById =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/nghinhut.comstar.Comstar/GetUserById',
      request,
      metadata || {},
      methodDescriptor_Comstar_GetUserById);
};


module.exports = proto.nghinhut.comstar;

