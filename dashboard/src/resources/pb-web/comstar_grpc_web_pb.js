/**
 * @fileoverview gRPC-Web generated client stub for nghinhut.comstar
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var google_protobuf_any_pb = require('google-protobuf/google/protobuf/any_pb.js')

var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')

var google_protobuf_duration_pb = require('google-protobuf/google/protobuf/duration_pb.js')

var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js')
const proto = {};
proto.nghinhut = {};
proto.nghinhut.comstar = require('./comstar_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.nghinhut.comstar.ComstarClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.nghinhut.comstar.ComstarPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.nghinhut.comstar.SubscriptionRequest,
 *   !proto.nghinhut.comstar.Event>}
 */
const methodDescriptor_Comstar_Subscription = new grpc.web.MethodDescriptor(
  '/nghinhut.comstar.Comstar/Subscription',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.nghinhut.comstar.SubscriptionRequest,
  proto.nghinhut.comstar.Event,
  /** @param {!proto.nghinhut.comstar.SubscriptionRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.Event.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.nghinhut.comstar.SubscriptionRequest,
 *   !proto.nghinhut.comstar.Event>}
 */
const methodInfo_Comstar_Subscription = new grpc.web.AbstractClientBase.MethodInfo(
  proto.nghinhut.comstar.Event,
  /** @param {!proto.nghinhut.comstar.SubscriptionRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.Event.deserializeBinary
);


/**
 * @param {!proto.nghinhut.comstar.SubscriptionRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.nghinhut.comstar.Event>}
 *     The XHR Node Readable Stream
 */
proto.nghinhut.comstar.ComstarClient.prototype.subscription =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/nghinhut.comstar.Comstar/Subscription',
      request,
      metadata || {},
      methodDescriptor_Comstar_Subscription);
};


/**
 * @param {!proto.nghinhut.comstar.SubscriptionRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.nghinhut.comstar.Event>}
 *     The XHR Node Readable Stream
 */
proto.nghinhut.comstar.ComstarPromiseClient.prototype.subscription =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/nghinhut.comstar.Comstar/Subscription',
      request,
      metadata || {},
      methodDescriptor_Comstar_Subscription);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.nghinhut.comstar.Event,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Comstar_PublishAnEvent = new grpc.web.MethodDescriptor(
  '/nghinhut.comstar.Comstar/PublishAnEvent',
  grpc.web.MethodType.UNARY,
  proto.nghinhut.comstar.Event,
  google_protobuf_empty_pb.Empty,
  /** @param {!proto.nghinhut.comstar.Event} request */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.nghinhut.comstar.Event,
 *   !proto.google.protobuf.Empty>}
 */
const methodInfo_Comstar_PublishAnEvent = new grpc.web.AbstractClientBase.MethodInfo(
  google_protobuf_empty_pb.Empty,
  /** @param {!proto.nghinhut.comstar.Event} request */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @param {!proto.nghinhut.comstar.Event} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.nghinhut.comstar.ComstarClient.prototype.publishAnEvent =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/nghinhut.comstar.Comstar/PublishAnEvent',
      request,
      metadata || {},
      methodDescriptor_Comstar_PublishAnEvent,
      callback);
};


/**
 * @param {!proto.nghinhut.comstar.Event} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     A native promise that resolves to the response
 */
proto.nghinhut.comstar.ComstarPromiseClient.prototype.publishAnEvent =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/nghinhut.comstar.Comstar/PublishAnEvent',
      request,
      metadata || {},
      methodDescriptor_Comstar_PublishAnEvent);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.nghinhut.comstar.HealthClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.nghinhut.comstar.HealthPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.nghinhut.comstar.HealthCheckRequest,
 *   !proto.nghinhut.comstar.HealthCheckResponse>}
 */
const methodDescriptor_Health_Check = new grpc.web.MethodDescriptor(
  '/nghinhut.comstar.Health/Check',
  grpc.web.MethodType.UNARY,
  proto.nghinhut.comstar.HealthCheckRequest,
  proto.nghinhut.comstar.HealthCheckResponse,
  /** @param {!proto.nghinhut.comstar.HealthCheckRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.HealthCheckResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.nghinhut.comstar.HealthCheckRequest,
 *   !proto.nghinhut.comstar.HealthCheckResponse>}
 */
const methodInfo_Health_Check = new grpc.web.AbstractClientBase.MethodInfo(
  proto.nghinhut.comstar.HealthCheckResponse,
  /** @param {!proto.nghinhut.comstar.HealthCheckRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.HealthCheckResponse.deserializeBinary
);


/**
 * @param {!proto.nghinhut.comstar.HealthCheckRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.nghinhut.comstar.HealthCheckResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.nghinhut.comstar.HealthCheckResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.nghinhut.comstar.HealthClient.prototype.check =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/nghinhut.comstar.Health/Check',
      request,
      metadata || {},
      methodDescriptor_Health_Check,
      callback);
};


/**
 * @param {!proto.nghinhut.comstar.HealthCheckRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.nghinhut.comstar.HealthCheckResponse>}
 *     A native promise that resolves to the response
 */
proto.nghinhut.comstar.HealthPromiseClient.prototype.check =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/nghinhut.comstar.Health/Check',
      request,
      metadata || {},
      methodDescriptor_Health_Check);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.nghinhut.comstar.HealthCheckRequest,
 *   !proto.nghinhut.comstar.HealthCheckResponse>}
 */
const methodDescriptor_Health_Watch = new grpc.web.MethodDescriptor(
  '/nghinhut.comstar.Health/Watch',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.nghinhut.comstar.HealthCheckRequest,
  proto.nghinhut.comstar.HealthCheckResponse,
  /** @param {!proto.nghinhut.comstar.HealthCheckRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.HealthCheckResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.nghinhut.comstar.HealthCheckRequest,
 *   !proto.nghinhut.comstar.HealthCheckResponse>}
 */
const methodInfo_Health_Watch = new grpc.web.AbstractClientBase.MethodInfo(
  proto.nghinhut.comstar.HealthCheckResponse,
  /** @param {!proto.nghinhut.comstar.HealthCheckRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.nghinhut.comstar.HealthCheckResponse.deserializeBinary
);


/**
 * @param {!proto.nghinhut.comstar.HealthCheckRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.nghinhut.comstar.HealthCheckResponse>}
 *     The XHR Node Readable Stream
 */
proto.nghinhut.comstar.HealthClient.prototype.watch =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/nghinhut.comstar.Health/Watch',
      request,
      metadata || {},
      methodDescriptor_Health_Watch);
};


/**
 * @param {!proto.nghinhut.comstar.HealthCheckRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.nghinhut.comstar.HealthCheckResponse>}
 *     The XHR Node Readable Stream
 */
proto.nghinhut.comstar.HealthPromiseClient.prototype.watch =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/nghinhut.comstar.Health/Watch',
      request,
      metadata || {},
      methodDescriptor_Health_Watch);
};


module.exports = proto.nghinhut.comstar;

