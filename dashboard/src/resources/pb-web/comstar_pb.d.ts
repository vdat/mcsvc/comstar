import * as jspb from "google-protobuf"

import * as google_protobuf_any_pb from 'google-protobuf/google/protobuf/any_pb';
import * as google_protobuf_empty_pb from 'google-protobuf/google/protobuf/empty_pb';
import * as google_protobuf_duration_pb from 'google-protobuf/google/protobuf/duration_pb';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';

export class SubscriptionRequest extends jspb.Message {
  getEventTypesList(): Array<Event.Type>;
  setEventTypesList(value: Array<Event.Type>): void;
  clearEventTypesList(): void;
  addEventTypes(value: Event.Type, index?: number): void;

  getSince(): google_protobuf_duration_pb.Duration | undefined;
  setSince(value?: google_protobuf_duration_pb.Duration): void;
  hasSince(): boolean;
  clearSince(): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SubscriptionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SubscriptionRequest): SubscriptionRequest.AsObject;
  static serializeBinaryToWriter(message: SubscriptionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SubscriptionRequest;
  static deserializeBinaryFromReader(message: SubscriptionRequest, reader: jspb.BinaryReader): SubscriptionRequest;
}

export namespace SubscriptionRequest {
  export type AsObject = {
    eventTypesList: Array<Event.Type>,
    since?: google_protobuf_duration_pb.Duration.AsObject,
  }
}

export class Event extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getType(): Event.Type;
  setType(value: Event.Type): void;

  getMetadataMap(): jspb.Map<string, google_protobuf_any_pb.Any>;
  clearMetadataMap(): void;

  getData(): google_protobuf_any_pb.Any | undefined;
  setData(value?: google_protobuf_any_pb.Any): void;
  hasData(): boolean;
  clearData(): void;

  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;
  hasCreatedAt(): boolean;
  clearCreatedAt(): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Event.AsObject;
  static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
  static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Event;
  static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
}

export namespace Event {
  export type AsObject = {
    id: number,
    type: Event.Type,
    metadataMap: Array<[string, google_protobuf_any_pb.Any.AsObject]>,
    data?: google_protobuf_any_pb.Any.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }

  export enum Type { 
    LOG = 0,
    SMS_CREATED = 10,
    SMS_QUEUED = 20,
    SMS_SENDING = 21,
    SMS_SENT = 23,
    SMS_FAILED = 25,
    SMS_RECEIVED = 30,
    RUNNER_STATUS_CHANGES = 40,
  }
}

export class Log extends jspb.Message {
  getLevel(): Log.Level;
  setLevel(value: Log.Level): void;

  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Log.AsObject;
  static toObject(includeInstance: boolean, msg: Log): Log.AsObject;
  static serializeBinaryToWriter(message: Log, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Log;
  static deserializeBinaryFromReader(message: Log, reader: jspb.BinaryReader): Log;
}

export namespace Log {
  export type AsObject = {
    level: Log.Level,
    message: string,
  }

  export enum Level { 
    DEBUG = 0,
    INFO = 1,
    WARN = 2,
    ERROR = 3,
    TRACE = 4,
  }
}

export class Sms extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getRunnerId(): number;
  setRunnerId(value: number): void;

  getBody(): string;
  setBody(value: string): void;

  getRecipientNumber(): string;
  setRecipientNumber(value: string): void;

  getType(): Sms.Type;
  setType(value: Sms.Type): void;

  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;
  hasCreatedAt(): boolean;
  clearCreatedAt(): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Sms.AsObject;
  static toObject(includeInstance: boolean, msg: Sms): Sms.AsObject;
  static serializeBinaryToWriter(message: Sms, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Sms;
  static deserializeBinaryFromReader(message: Sms, reader: jspb.BinaryReader): Sms;
}

export namespace Sms {
  export type AsObject = {
    id: number,
    runnerId: number,
    body: string,
    recipientNumber: string,
    type: Sms.Type,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }

  export enum Type { 
    OUTGOING = 0,
    INCOMING = 1,
  }
}

export class SmsReceived extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SmsReceived.AsObject;
  static toObject(includeInstance: boolean, msg: SmsReceived): SmsReceived.AsObject;
  static serializeBinaryToWriter(message: SmsReceived, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SmsReceived;
  static deserializeBinaryFromReader(message: SmsReceived, reader: jspb.BinaryReader): SmsReceived;
}

export namespace SmsReceived {
  export type AsObject = {
  }
}

export class HealthCheckRequest extends jspb.Message {
  getService(): string;
  setService(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HealthCheckRequest.AsObject;
  static toObject(includeInstance: boolean, msg: HealthCheckRequest): HealthCheckRequest.AsObject;
  static serializeBinaryToWriter(message: HealthCheckRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HealthCheckRequest;
  static deserializeBinaryFromReader(message: HealthCheckRequest, reader: jspb.BinaryReader): HealthCheckRequest;
}

export namespace HealthCheckRequest {
  export type AsObject = {
    service: string,
  }
}

export class HealthCheckResponse extends jspb.Message {
  getStatus(): HealthCheckResponse.ServingStatus;
  setStatus(value: HealthCheckResponse.ServingStatus): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HealthCheckResponse.AsObject;
  static toObject(includeInstance: boolean, msg: HealthCheckResponse): HealthCheckResponse.AsObject;
  static serializeBinaryToWriter(message: HealthCheckResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HealthCheckResponse;
  static deserializeBinaryFromReader(message: HealthCheckResponse, reader: jspb.BinaryReader): HealthCheckResponse;
}

export namespace HealthCheckResponse {
  export type AsObject = {
    status: HealthCheckResponse.ServingStatus,
  }

  export enum ServingStatus { 
    UNKNOWN = 0,
    SERVING = 1,
    NOT_SERVING = 2,
  }
}

