## CORE Component only

## build
FROM node:10-alpine
COPY core/package*.json ./
RUN npm install
COPY core/ ./
RUN npm run build


## release
FROM node:10-alpine

COPY core/package*.json ./
RUN npm install --production
COPY --from=0 dist ./dist/

EXPOSE 3001 5000

CMD ["npm", "run", "start:prod"]
