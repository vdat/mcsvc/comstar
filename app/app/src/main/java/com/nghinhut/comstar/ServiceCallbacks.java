package com.nghinhut.comstar;

public interface ServiceCallbacks {
    void doSomething();
    void newSmsCallback(Sms sms);
}
