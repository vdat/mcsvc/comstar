package com.nghinhut.comstar;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PersistableBundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;

public class MainActivity extends AppCompatActivity implements ServiceCallbacks {
    boolean bound;

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int APP_PERMISSIONS_REQUEST_ALL = 1;
    private static final int APP_PERMISSIONS_REQUEST_SEND_SMS = 2;
    private static final int APP_PERMISSIONS_REQUEST_SMS_RECEIVE = 3;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = this.getApplicationContext();
        Sentry.init(
                "https://f23476d587e54c2d9ad45f757878262b@sentry.io/1723773",
                new AndroidSentryClientFactory(ctx)
        );

        setContentView(R.layout.activity_main);

        try {
            // Check to see if SMS is enabled.
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Permission not granted");
                // Permission not yet granted. Use requestPermissions().
                // MY_PERMISSIONS_REQUEST_SEND_SMS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
                requestPermissions(
                        new String[]{
                                Manifest.permission.SEND_SMS,
                                Manifest.permission.RECEIVE_SMS,
                                Manifest.permission.INTERNET,
                                Manifest.permission.WAKE_LOCK,
                                Manifest.permission.RECEIVE_BOOT_COMPLETED
                        },
                        APP_PERMISSIONS_REQUEST_ALL);

//                // Permission is not granted
//                // Should we show an explanation?
//                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                        Manifest.permission.SEND_SMS)) {
//                    // Show an explanation to the user *asynchronously* -- don't block
//                    // this thread waiting for the user's response! After the user
//                    // sees the explanation, try again to request the permission.
//                } else {
//                    // No explanation needed; request the permission
//                    ActivityCompat.requestPermissions(this,
//                            new String[]{Manifest.permission.SEND_SMS},
//                            MY_PERMISSIONS_REQUEST_SEND_SMS);
//
//                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                    // app-defined int constant. The callback method gets the
//                    // result of the request.
//                }
            } else {
                // Permission already granted. Enable SMS Service.
                Log.d(TAG, "Permission granted");
                startService();
            }

        } catch (Exception e) {
            Sentry.capture(e);
        }

//        sendSMS("0968927456", " sdfasdfasdf sdfasdfasdfasd sdfasdfasdf asdf asdfas df asdf asd fas dfa sdf asd fas dfs dfadfasdvasvdasdvasvasdvao asdfas nvdnasdvjkasnviasdjvbasjhvbaskjvdhasdbjfasdf");
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (bound) {
            unbindService(mConnection);
            bound = false;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d(TAG, "onServiceConnected");
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.

            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ComstarService.LocalBinder binder = (ComstarService.LocalBinder) service;
            bound = true;
            binder.getService().setCallbacks(MainActivity.this); // register
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            bound = false;
        }
    };

    private void startService() {
//        Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "startService");
        Intent intent = new Intent(this, ComstarService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
//        finish();

//        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
//        jobScheduler.schedule(new JobInfo.Builder(1,
//                new ComponentName(this, ComstarJobService.class))
//                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                .setPersisted(true)
//                .setPeriodic(5000)
//                .build());
    }

    public void addJob() {
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(new JobInfo.Builder(1,
                new ComponentName(this, ComstarJobService.class))
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        switch (requestCode) {
            case APP_PERMISSIONS_REQUEST_ALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    startService();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

//            case APP_PERMISSIONS_REQUEST_SMS_RECEIVE: {
//
//            }
        }


//        if (requestCode == APP_PERMISSIONS_REQUEST_SEND_SMS
//                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            startService();
//        }
    }

    @Override
    public void doSomething() {
//        Toast.makeText(this, "doSomething()", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "doSomething()");
    }


    @Override
    public void newSmsCallback(Sms sms) {
        PersistableBundle bundle = new PersistableBundle();
        bundle.putString("grpc_message_name", Sms.class.getSimpleName());

        bundle.putInt("id", (int) sms.getId());
        bundle.putString("message", sms.getBody());

        sendSMS(sms.getRecipientNumber(), sms.getBody());

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(new JobInfo.Builder((int) sms.getId(),
                new ComponentName(this, ComstarJobService.class))
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .setMinimumLatency(1 * 1000)
                .setOverrideDeadline(10 * 1000)
//                .setPeriodic(5000)
                .setExtras(bundle)
                .build());
    }

    private void sendSMS(String phoneNumber, String message)
    {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                Log.d("onReceive", "" + getResultCode());
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                Log.d("onReceive", "onReceive " + getResultCode());
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }
}
