package com.nghinhut.comstar;

import android.app.job.JobParameters;
import android.os.PersistableBundle;
import android.util.Log;
import android.widget.Toast;

public class ComstarJobService extends android.app.job.JobService {
    private static final String TAG = ComstarJobService.class.getSimpleName();

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Log.d(TAG, "onStartJob");

        PersistableBundle persistableBundle = jobParameters.getExtras();

        String jobType = persistableBundle.getString("grpc_message_name");

        if (jobType == null || jobType.length() == 0) { return false; }

        switch (jobType) {
            case "Sms": {
                Sms sms = Sms.newBuilder()
                        .setId(persistableBundle.getInt("id"))
                        .setBody(persistableBundle.getString("message"))
                        .build();

                Toast.makeText(this, sms.getBody(), Toast.LENGTH_SHORT).show();
                break;
            }
            default:
                throw new IllegalStateException("Unexpected value: " + jobType);
        }

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
