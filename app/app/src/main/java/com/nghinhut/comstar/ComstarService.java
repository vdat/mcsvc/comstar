package com.nghinhut.comstar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.PersistableBundle;
import android.os.PowerManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.text.MessageFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class ComstarService extends Service {
    private static final String TAG = ComstarService.class.getSimpleName();
    private final IBinder iBinder = new LocalBinder();
    PowerManager.WakeLock wakeLock;
    WifiManager.WifiLock wifiLock;
    SmsManager smsManager = SmsManager.getDefault();
    private ManagedChannel channel;
    private WeakReference<MainActivity> mainActivityWeakReference;
    Messenger mMessenger;

    private StreamObserver<Event> streamObserver;
    private Throwable failed;
    private CountDownLatch streamFinishedCountDownLatch;

    ComstarService self;


    private ServiceCallbacks serviceCallbacks;


    private Handler handler;
    public static final long DEFAULT_INTERVAL = 5 * 1000;

    // Messenger commands
    static final int MSG_SAY_HELLO = 1;
    // ----

    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    public synchronized static String id(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
//                editor.commit();
                editor.apply();
            }
        }
        return uniqueID;
    }

    public StreamObserver<Event> createStreamEventStreamObserver() {
        this.failed = null;
        this.streamFinishedCountDownLatch = new CountDownLatch(1);

        return ComstarGrpc.newStub(channel).streamEventsStream(
                new StreamObserver<Event>() {
                    @Override
                    public void onNext(Event event) {
//                        self.doSomething();

                        Log.d("NEW EVENT", event.getType().toString());
                        Any eventData = null;
                        try {
                            eventData = Any.parseFrom(event.getData().toByteString());
                        } catch (InvalidProtocolBufferException e) {
                            e.printStackTrace();
                        }

                        if (eventData == null) { return; }

                        switch (eventData.getTypeUrl()) {
                            case "type.googleapis.com/nghinhut.comstar.Log":{
                                com.nghinhut.comstar.Log log = null;
                                try {
                                    log = com.nghinhut.comstar.Log.parseFrom(event.getData().getValue().toByteArray());
                                } catch (InvalidProtocolBufferException e) {
                                    e.printStackTrace();
                                } finally {
                                    Log.d("gRPC " + log.getLevel().toString(), log.getMessage());


                                    // send an ev to server
                                    com.nghinhut.comstar.Log log2 = com.nghinhut.comstar.Log.newBuilder()
                                            .setLevel(com.nghinhut.comstar.Log.Level.INFO)
                                            .setMessage("[" + id(getApplicationContext()) + "] " + (System.currentTimeMillis()/1000))
                                            .build();


//                                    Any any = Any.newBuilder()
//                                            .setTypeUrl("nghinhut.comstart.Log")
//                                            .setValue(com.nghinhut.comstar.Log.getDefaultInstance().toByteArray())
//                                            .build();

                                    Any data = Any.newBuilder()
                                            .setTypeUrl("nghinhut.comstar.Log")
                                            .setValue(log2.toByteString()).build();


                                    Event ev = Event.newBuilder()
                                            .setType(Event.Type.LOG)
                                            .setData(data)
                                            .build();
                                    pulishAnEvent(ev);
                                }
                                break;
                            }

                            case "type.googleapis.com/nghinhut.comstar.Sms": {
                                try {

                                    Sms sms = Sms.parseFrom(eventData.getValue().toByteArray());
//                                    newSmsCallback(sms);
//                                    sendSms(sms);
                                    sendSMS(sms.getRecipientNumber(), sms.getBody());
                                } catch(Exception e) {
                                    Log.d("onNext", e.toString());
                                }
                            }

                            default:
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        failed = t;
                        streamFinishedCountDownLatch.countDown();
                    }

                    @Override
                    public void onCompleted() {
                        streamFinishedCountDownLatch.countDown();
                    }
                });
    }

    public void doSomething() {
        this.serviceCallbacks.doSomething();
    }

    public void newSmsCallback(Sms sms) {
        serviceCallbacks.newSmsCallback(sms);
    }

    private Runnable runnableService = new Runnable() {
        @Override
        public void run() {
            //create AsyncTask here


            try {
                polling();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            checkChannelConntection();

            handler.postDelayed(runnableService, DEFAULT_INTERVAL);
        }
    };

    public void pulishAnEvent(Event event) {
//        Log.d(TAG, "pulishAnEvent");
        this.streamObserver.onNext(event);
    }

    public void setCallbacks(ServiceCallbacks callbacks) { serviceCallbacks = callbacks; }

    public class LocalBinder extends Binder {
        ComstarService getService() { return ComstarService.this; }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        Toast.makeText(getApplicationContext(), "binding", Toast.LENGTH_SHORT).show();
        return iBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Error", "Start");
//        this.mainActivityWeakReference;
//        Toast.makeText(this, "service: onCreate()", Toast.LENGTH_SHORT).show();

    }

    @SuppressLint("WakelockTimeout")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("DEBUG", "onStartCommand");
        self = this;

        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "comstar:");
        }
        wakeLock.acquire();

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, "comstar");
        }
        wifiLock.acquire();

        this.channel = ManagedChannelBuilder.forAddress(
                "10.0.2.2",10000
        ).usePlaintext().build();


//        Handler sdf = new Handler();
//        adf = new Runnable() {
//            @Override
//            public void run() {
//                new ComstarTask(new TimestampRunnable(), channel, self).execute();
//
//                sdf.postDelayed(adf, DEFAULT_INTERVAL);
//            }
//        };
//
//        sdf.post(adf);


        handler = new Handler();
        handler.post(runnableService);
//        polling();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        this.channel.shutdownNow();
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
        Log.d("ERROR", "Destroy");
    }

    public boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    private static class ComstarTask extends AsyncTask<Void, Void, String> {
        private final ComstarRunnable comstarRunnable;
        private final WeakReference<ComstarService> comstarServiceWeakReference;
        private final ManagedChannel channel;

        ComstarTask(ComstarRunnable comstarRunnable, ManagedChannel channel, ComstarService comstarService) {
            this.comstarRunnable = comstarRunnable;
            channel.getState(true);
            this.channel = channel;

            this.comstarServiceWeakReference = new WeakReference<>(comstarService);

        }

        @Override
        protected String doInBackground(Void... nothing) {
            try {
//                if (Context.isConnected(serviceWeakReference.get())) { return "no internet connection"; }

                return comstarRunnable.run(comstarServiceWeakReference.get(), channel);

//                return null;

            } catch (Exception e) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                pw.flush();
                return "Failed... : " + sw;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(comstarServiceWeakReference.get().getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        }
    }

    private interface ComstarRunnable {
        /**
         * Perform a grpcRunnable and return all the logs.
         */
        String run(ComstarService comstarService, ManagedChannel channel) throws Exception;
    }

    private static class PollingRunnable implements ComstarRunnable {
        private Throwable failed;
        private Object streamObserver;
        private CountDownLatch streamFinishedCountDownLatch;
//        @Override
//        public String run() throws Exception {
//            return "polling";
//        }

        @Override
        public String run(ComstarService comstarService, ManagedChannel channel) throws Exception {
//            this.streamObserver = comstarService.createStreamEventStreamObserver();
//            this.streamFinishedCountDownLatch.await();
//            return "polling";
            return streamEventsStream(comstarService, channel);
//            return null;
        }

        private String streamEventsStream(final ComstarService comstarService, ManagedChannel channel) throws InterruptedException, RuntimeException  {
            Log.d("streamJobStream", "START");
            final StringBuffer logs = new StringBuffer();
//            appendLogs(logs, "*** RouteChat");
//            channel.getState(true);
            final CountDownLatch finishLatch = new CountDownLatch(1);
            StreamObserver<Event> requestObserver = ComstarGrpc.newStub(channel).streamEventsStream(
                    new StreamObserver<Event>() {
                        @Override
                        public void onNext(Event event) {
//                        self.doSomething();

                            Log.d("NEW EVENT", event.getType().toString());
                            Any eventData = null;
                            try {
                                eventData = Any.parseFrom(event.getData().toByteString());
                            } catch (InvalidProtocolBufferException e) {
                                e.printStackTrace();
                            }

                            if (eventData == null) { return; }

                            switch (eventData.getTypeUrl()) {
                                case "type.googleapis.com/nghinhut.comstar.Log":{
                                    com.nghinhut.comstar.Log log = null;
                                    try {
                                        log = com.nghinhut.comstar.Log.parseFrom(event.getData().getValue().toByteArray());
                                    } catch (InvalidProtocolBufferException e) {
                                        e.printStackTrace();
                                    } finally {
                                        Log.d("gRPC " + log.getLevel().toString(), log.getMessage());


                                        // send an ev to server
                                        com.nghinhut.comstar.Log log2 = com.nghinhut.comstar.Log.newBuilder()
                                                .setLevel(com.nghinhut.comstar.Log.Level.INFO)
                                                .setMessage("[" + id(comstarService.getApplicationContext()) + "] " + (System.currentTimeMillis()/1000))
                                                .build();


//                                    Any any = Any.newBuilder()
//                                            .setTypeUrl("nghinhut.comstart.Log")
//                                            .setValue(com.nghinhut.comstar.Log.getDefaultInstance().toByteArray())
//                                            .build();

//                                        Any data = Any.newBuilder()
//                                                .setTypeUrl("nghinhut.comstar.Log")
//                                                .setValue(log2.toByteString()).build();
//
//
//                                        Event ev = Event.newBuilder()
//                                                .setType(Event.Type.LOG)
//                                                .setData(data)
//                                                .build();
//                                        pulishAnEvent(ev);
                                    }
                                    break;
                                }

                                case "type.googleapis.com/nghinhut.comstar.Sms": {
                                    try {

                                        Sms sms = Sms.parseFrom(eventData.getValue().toByteArray());
//                                    newSmsCallback(sms);
//                                        comstarService.sendSms(sms);
                                        comstarService.sendSMS(sms.getRecipientNumber(), sms.getBody());
                                    } catch(Exception e) {
                                        Log.d("onNext", e.toString());
                                    }
                                }

                                default:
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            failed = t;
                            finishLatch.countDown();
                        }

                        @Override
                        public void onCompleted() {
                            finishLatch.countDown();
                        }
                    });
//            try {
//                Job[] requests = {
//                        newJob("First message"),
//                        newJob("Second message"),
//                        newJob("Third message"),
//                        newJob("Fourth message")
//                };
//
//                for (Job request : requests) {
//                    appendLogs( logs, "Sending message \"{0}\" at", request.getMessage());
//                    requestObserver.onNext(request);
//                }
//            } catch (RuntimeException e) {
//                // Cancel RPC
//                requestObserver.onError(e);
//                throw e;
//            }
            // Mark the end of requests
//            requestObserver.onCompleted();

            // Receiving happens asynchronously
            finishLatch.await();
//            if (!finishLatch.await(1, TimeUnit.MINUTES)) {
//                throw new RuntimeException("Could not finish rpc within 1 minute, the server is likely down");
//            }

            if (failed != null) {
//                new ComstarTask(new PollingRunnable(), channel, comstarService).execute();
                String errorMessage = failed.getMessage();
                Log.d("streamJobStream:failed", errorMessage);
//                throw new RuntimeException(failed);

                logs.append(errorMessage);
            }


            return logs.toString();
//            return null;
        }
    }

    private static class TimestampRunnable implements ComstarRunnable {

        @Override
        public String run(ComstarService comstarService, ManagedChannel channel) throws Exception {
            com.nghinhut.comstar.Log log = com.nghinhut.comstar.Log.newBuilder()
                    .setLevel(com.nghinhut.comstar.Log.Level.INFO)
                    .setMessage("[" + id(comstarService.getApplicationContext()) + "] " +new Date().toString())
                    .build();
            Any any = Any.newBuilder()
                    .setTypeUrl("nghinhut.comstart.Log")
                    .setValue(log.getMessageBytes())
                    .build();

            Event event = Event.newBuilder()
                    .setType(Event.Type.LOG)
                    .setData(any)
                    .build();
            comstarService.pulishAnEvent(event);

            return null;
        }
    }

    private static void appendLogs(StringBuffer logs, String msg, Object... params) {
        if (params.length > 0) logs.append(MessageFormat.format(msg, params)); else logs.append(msg);
        logs.append("\n");
    }

    private static Sms newSms(String message) { return Sms.newBuilder().setBody(message).build(); }

    public void polling() throws InterruptedException {
//        this.streamObserver = this.createStreamEventStreamObserver();
//        this.streamFinishedCountDownLatch.await();
//        Toast.makeText(this, this.failed.getMessage(), Toast.LENGTH_SHORT).show();
        new ComstarTask(new PollingRunnable(), channel, this).execute();
    }

//    public void checkChannelConntection() {
//        new ComstarTask(new ComstarRunnable() {
//            @Override
//            public String run(ComstarService comstarService, ManagedChannel channel) throws Exception {
//                Log.d("checkChannelConntection", "checkChannelConntection");
//                Log.d("checkChannelConntection", channel.getState(false).toString());
////                if (channel.getState(false).toString()) {
////
////                };
//                if (!channel.getState(false).toString().contentEquals("READY")) {
//                    channel.getState(true);
//                    polling();
//                }
//                return null;
//            }
//        }, channel, this).execute();
//    }

    public void sendSms(Sms sms) {
        Log.d(TAG, "call SendSMSJobScheduler");
        PersistableBundle bundle = new PersistableBundle();
        bundle.putString("grpc_message_name", Sms.class.getSimpleName());

        bundle.putInt("id", (int) sms.getId());
        bundle.putString("message", sms.getBody());

        try {
            JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
            jobScheduler.schedule(new JobInfo.Builder((int) sms.getId(),
                    new ComponentName(this, ComstarJobService.class))
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPersisted(true)
                    .setMinimumLatency(1 * 1000)
                    .setOverrideDeadline(10 * 1000)
//                .setPeriodic(5000)
                    .setExtras(bundle)
                    .build());
        } catch(Exception e) {
            Log.d("sendSms", e.toString());
        }
    }

    private void sendSMS(String phoneNumber, String message)
    {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                Log.d("onReceive", "" + getResultCode());
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "SMS sent");
//                        Toast.makeText(getBaseContext(), "SMS sent",
//                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Log.d(TAG, "Generic failure");
//                        Toast.makeText(getBaseContext(), "Generic failure",
//                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Log.d(TAG, "No service");
//                        Toast.makeText(getBaseContext(), "No service",
//                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Log.d(TAG, "Null PDU");
//                        Toast.makeText(getBaseContext(), "Null PDU",
//                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Log.d(TAG, "Radio off");
//                        Toast.makeText(getBaseContext(), "Radio off",
//                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                Log.d("onReceive", "" + getResultCode());
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "SMS delivered");
//                        Toast.makeText(getBaseContext(), "SMS delivered",
//                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.d(TAG, "SMS not delivered");
//                        Toast.makeText(getBaseContext(), "SMS not delivered",
//                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }
}





