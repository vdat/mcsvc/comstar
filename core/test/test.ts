import {ComstarClient} from '../src/resources/pb-node/comstar_grpc_pb';
import {SubscriptionRequest, Event} from '../src/resources/pb-node/comstar_pb';
import * as grpc from 'grpc';

const client = new ComstarClient('localhost:10000', grpc.credentials.createInsecure(), null);

const stream = client.subscription(new SubscriptionRequest(), null );

stream.on('data', data => {
  console.log(data);
})

stream.on('status', status => {
  console.log(status);
})


// const stream2 = client.streamEventsStream();
//
// stream2.on('data', data => {
//   console.log(data);
// })
