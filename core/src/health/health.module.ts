import { Module } from '@nestjs/common';
import {
  TerminusModule,
  TerminusModuleOptions,
  MicroserviceHealthIndicator,
  GRPCHealthIndicator,
} from '@nestjs/terminus';
import {join} from 'path';

const getTerminusOptions = (
  grpc: GRPCHealthIndicator,
): TerminusModuleOptions => ({
  endpoints: [
    {
      url: '/health',
      healthIndicators: [
        async () =>
          grpc.checkService('comstar', 'nghinhut.comstar.Comstar', {
            timeout: 2000,
            package: 'nghinhut.comstar',
            protoPath: 'comstar.proto',
            loader: {includeDirs: [join(__dirname, '..')]},
          }),
      ],
    },
  ],
});

@Module({
  imports: [
    TerminusModule.forRootAsync({
      inject: [GRPCHealthIndicator],
      useFactory: getTerminusOptions,
    }),
  ],
})
export class HealthModule {}
