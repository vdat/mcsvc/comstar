import {Controller, Logger} from '@nestjs/common';
import {JobsService} from '../jobs/jobs.service';
import {GrpcMethod, GrpcStreamMethod} from '@nestjs/microservices';

enum ServingStatus {
  UNKNOWN = 0,
  SERVING = 1,
  NOT_SERVING = 2,
}

interface HealthCheckRequest {
  service: string;
}

interface HealthCheckResposne {
  status: ServingStatus;
}

@Controller()
export class Health {
  private readonly logger = new Logger(this.constructor.name);

  @GrpcMethod()
  check(healthCheckRequest: HealthCheckRequest, metadata: any) {
    this.logger.debug(healthCheckRequest);
    return { status: ServingStatus.SERVING } as HealthCheckResposne;
  }

  @GrpcMethod()
  watch(healthCheckRequest: HealthCheckRequest, metadata: any) {
    this.logger.debug(healthCheckRequest);
    return { status: ServingStatus.SERVING } as HealthCheckResposne;
  }
}
