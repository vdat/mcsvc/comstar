import {Inject, Injectable, Logger} from '@nestjs/common';
import {User} from './interfaces/event.interface';
import {CreateEventDto} from './dto/create-event.dto';
import { Model } from 'mongoose';
import {SubscriptionRequest, Event, Log, Sms} from '../resources/pb-node/comstar_pb';
import {interval, of, Subject} from 'rxjs';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as google_protobuf_any_pb from 'google-protobuf/google/protobuf/any_pb';
import * as google_protobuf_empty_pb from 'google-protobuf/google/protobuf/empty_pb';
import * as _ from 'lodash';
import {concatMap, delay} from 'rxjs/operators';

@Injectable()
export class EventsService {
  private readonly logger = new Logger(this.constructor.name);
  private eventSubject = new Subject<any>();
  public event$ = this.eventSubject.asObservable();

  constructor(@Inject('EVENT_MODEL') private readonly eventModel: Model<Event>) {
    // Server Time
    interval(3000).subscribe(() => {
      const log = new Log();
      const ev = new Event();

      log.setLevel(Log.Level.INFO);
      log.setMessage(`[${process.env.HOSTNAME.slice(0, 4)}] ${new Date().getTime().toString()}`);

      ev.setType(Event.Type.LOG);
      const data = new google_protobuf_any_pb.Any();
      data.pack(log.serializeBinary(), 'nghinhut.comstar.Log');
      ev.setData(data);

      // const result = ev.toObject();
      // result.data = log.toObject();
      this.emit(ev);
    });

    // // SMS
    // interval(2000)
    //   .pipe(
    //     concatMap(i => of(i).pipe(delay(_.random(1000, 60000)))),
    //   )
    //   .subscribe(() => {
    //   const sms = new Sms();
    //   sms.setRecipientNumber('123123');
    //   sms.setBody('hi');
    //
    //   this.emit(sms);
    // });
  }

  async create(createDto: CreateEventDto): Promise<User> {
    const created = new this.eventModel(createDto);
    return await created.save();
  }

  findEventStream(request: SubscriptionRequest.AsObject, onData, onClose) {
    const cursor = this.eventModel.find({ kind: {$in: [request.eventTypesList.forEach(kind => kind.toString)]} }).cursor();
    cursor.on('data', onData);
    cursor.on('close', onClose);
  }

  emit(o: Log | Sms | Event) {
    if (!o) { throw Error('event cannot be undefined or null!'); }
    let event: Event;
    if (o instanceof Log) {
      event = new Event();
      event.setType(Event.Type.LOG);

      const data = new google_protobuf_any_pb.Any();
      data.pack(o.serializeBinary(), 'nghinhut.comstar.Log');
      event.setData(data);
      this.logger.log('Log');
    } else if (o instanceof Sms) {
      event = new Event();
      event.setType(Event.Type.SMS_CREATED);

      const data = new google_protobuf_any_pb.Any();
      data.pack(o.serializeBinary(), 'nghinhut.comstar.Sms');
      event.setData(data);
      this.logger.log('Sms');
    } else if (o instanceof Event) {
      this.logger.log('event');
      event = o;
    }

    // this.logger.log(event.getData().getTypeUrl()); // type.googleapis.com/nghinhut.comstar.*
    // this.logger.log(event.getData().getTypeName()); // nghinhut.comstar.*
    if (event.getType() === Event.Type.LOG && event.getData().getTypeUrl() === 'type.googleapis.com/nghinhut.comstar.Log') {
      // const data = Log.deserializeBinary(new Uint8Array(event.getData()));
      const data = event.getData();
      const log = Log.deserializeBinary(data.getValue_asU8());
      this.logger.log(`${log.getLevel()} ${log.getMessage()}`);
    }
    this.eventSubject.next(event);
  }

  // async findOne(username: string): Promise<User | undefined> {
  //   return this.users.find(user => user.username === username);
  // }
}
