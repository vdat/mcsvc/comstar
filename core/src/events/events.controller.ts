import {Controller, Get, Post, Body, Logger} from '@nestjs/common';
import {GrpcMethod, GrpcService, GrpcStreamCall, GrpcStreamMethod} from '@nestjs/microservices';
import {BehaviorSubject, fromEvent, Observable, Subject} from 'rxjs';
import {Event, SubscriptionRequest, Log, Sms} from '../resources/pb-node/comstar_pb';
import {EventsService} from './events.service';
import * as google_protobuf_any_pb from 'google-protobuf/google/protobuf/any_pb';
import * as google_protobuf_empty_pb from 'google-protobuf/google/protobuf/empty_pb';
import {catchError, filter, map, takeUntil} from 'rxjs/operators';
import * as _ from 'lodash';
import {CANCEL_EVENT} from '@nestjs/microservices/constants';
import {Server, CustomTransportStrategy} from '@nestjs/microservices';

@Controller()
export class EventsController {
  private readonly logger = new Logger(this.constructor.name);

  constructor(
    private readonly eventsService: EventsService,
  ) {}

  parseEvent(object: any): Event {
    object = _.mapKeys(object, _.rearg(_.camelCase, 1));
    object.data = _.mapKeys(object.data, _.rearg(_.camelCase, 1));
    object.type = object.type || 0;

    const eventObject: Event.AsObject = object;
    const eventData: google_protobuf_any_pb.Any = new google_protobuf_any_pb.Any();
    eventData.setTypeUrl(eventObject.data.typeUrl);
    eventData.setValue(eventObject.data.value.toString());
    const event = new Event();
    // log = Log.deserializeBinary(Uint8Array.from(Buffer.from(event.data.value.toString())));
    this.logger.log(eventObject);
    // default is LOG
    const objectData = object.data;
    // const eventDataValue: {type: string, data: any[]} = eventData.value;
    switch (eventData.getTypeUrl()) {
      case 'type.googleapis.com/nghinhut.comstar.Log': {
        let log: Log;
        try {
          log = Log.deserializeBinary(Uint8Array.from(Buffer.from(object.data.value.toString())));
        } catch (e) {
          throw e;
        }
        this.logger.log(log.getMessage());

        // this.logger.log(Log.deserializeBinary(event.data.value).getMessage());
        // this.logger.log(data.toObject());
        // const data: Log = Log.deserializeBinary(event.data && event.data.value);

        const data = new google_protobuf_any_pb.Any();
        data.pack(log.serializeBinary(), 'nghinhut.comstar.Log');

        event.setType(Event.Type.LOG);
        event.setData(data);
        break;
      }

      case 'type.googleapis.com/nghinhut.comstar.Sms': {
        try {
          const sms: Sms = Sms.deserializeBinary(Uint8Array.from(Buffer.from(object.data.value)));
          const data = new google_protobuf_any_pb.Any();
          data.pack(sms.serializeBinary(), 'nghinhut.comstar.Sms');
          event.setType(Event.Type.SMS_CREATED);
          event.setData(data);
        } catch (e) {
          throw e;
        }
        break;
      }

      // case Event.Type.SMS_STATUS_CHANGES: {
      //   break;
      // }

      // case Event.Type.: {
      //
      // }

      default: throw Error('Unknown Event.Type = ' + eventData.getTypeUrl());
    }

    return event;
  }

  @GrpcMethod('Comstar')
  publishAnEvent(object: any) {
    const event: Event = this.parseEvent(object);
    this.eventsService.emit(event);
    return new google_protobuf_empty_pb.Empty().toObject();
  }

  @GrpcMethod('Comstar')
  async subscription(req: any): Promise<any> {
    return this.eventsService.event$
      .pipe(
        map((event: Event) => {
          const obj = _.mapKeys(event.toObject(), _.rearg(_.snakeCase, 1));
          obj.data = _.mapKeys(obj.data, _.rearg(_.snakeCase, 1));
          return obj;
        }),
      );
  }

  @GrpcStreamMethod('Comstar')
  async streamEventsStream(messages: Observable<any>): Promise<any> {
    const s = new Subject();
    const o = s.asObservable();

    this.eventsService.emit((() => {
      const log = new Log();
      log.setLevel(Log.Level.TRACE);
      log.setMessage('start');
      return log;
    })());

    messages.subscribe(
      (event: Event.AsObject) => {
        event.type = event.type || Event.Type.LOG; // default is LOG
        // const eventDataValue: {type: string, data: any[]} = eventData.value;
        switch (event.type) {
          case Event.Type.LOG: {
            let log: Log;
            try {
              log = Log.deserializeBinary(Uint8Array.from(Buffer.from(event.data.value.toString())));
            } catch (e) {
              throw e;
            }
            this.logger.log(log.getMessage());

            // this.logger.log(Log.deserializeBinary(event.data.value).getMessage());
            // this.logger.log(data.toObject());
            // const data: Log = Log.deserializeBinary(event.data && event.data.value);

            break;
          }

          // case Event.Type.SMS_STATUS_CHANGES: {
          //   break;
          // }

          // case Event.Type.: {
          //
          // }

          default: this.logger.error('Unknown Event.Type = ' + event.type);
        }
        // this.logger.log(msg);
      },
      error => {
        const log = new Log();
        log.setLevel(Log.Level.ERROR);
        log.setMessage(error.toString());

        this.eventsService.emit(log);
      },
      () => {
        const log = new Log();
        log.setLevel(Log.Level.TRACE);
        log.setMessage('complete');

        this.eventsService.emit(log);
      },
    );

    this.eventsService.event$.subscribe((event: Event) => {
      const obj = _.mapKeys(event.toObject(), _.rearg(_.snakeCase, 1));
      obj.data = _.mapKeys(obj.data, _.rearg(_.snakeCase, 1));
      s.next(obj);
    });

    return o;
  }
}
