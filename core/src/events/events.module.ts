import { Module } from '@nestjs/common';
import { EventsService } from './events.service';
import {DatabaseModule} from '../database/database.module';
import {eventsProviders} from './events.provider';
import {EventsController} from './events.controller';

@Module({
  imports: [DatabaseModule],
  providers: [EventsService, ...eventsProviders],
  exports: [EventsService],
  controllers: [EventsController],
})
export class EventsModule {}
