import * as mongoose from 'mongoose';

export const EventSchema = new mongoose.Schema({
  id: String,
  identities: {
    type: Map,
    of: {
      iss: String,
      sub: String,
    },
  },
});
