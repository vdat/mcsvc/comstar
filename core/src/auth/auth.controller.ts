import {Controller, Get, HttpService, Logger, Post, Render, Req, Res, UseGuards} from '@nestjs/common';
import {Request, Response, urlencoded} from 'express';
import { AuthGuard } from '@nestjs/passport';
import {AuthService} from './auth.service';
import * as _ from 'lodash';
import * as sjcl from 'sjcl';
import {delay, map, retry, retryWhen, take} from 'rxjs/operators';
import {JwtService} from '@nestjs/jwt';
import {UsersService} from '../users/users.service';
import {IdToken} from '../users/interfaces/id-token.interface';

interface CodeResponse {
  code: string;
  scope: string;
  authuser: string;
  session_state: string;
  prompt: string;
}

interface TokenResponse {
  access_token: string;
  expires_in: string;
  scope: string;
  token_type: string;
  id_token: string;
}

interface OpenIdConfig {
  issuer: string;
  authorization_endpoint: string;
  token_endpoint: string;
  userinfo_endpoint: string;
  revocation_endpoint: string;
  jwks_uri: string;
  response_types_supported: string[];
  subject_types_supported: string[];
  id_token_signing_alg_values_supported: string[];
  scopes_supported: string[];
  token_endpoint_auth_methods_supported: string[];
  claims_supported: string[];
  code_challenge_methods_supported: string[];
}

const CLIENT_ID = process.env.CLIENT_ID || '';
const CLIENT_SECRET = process.env.CLIENT_SECRET || '';

@Controller()
export class AuthController {
  private readonly logger = new Logger(this.constructor.name);
  private openidConfig: OpenIdConfig;

  constructor(
    private readonly authService: AuthService,
    private readonly http: HttpService,
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) {
    this.http.get('https://accounts.google.com/.well-known/openid-configuration', {responseType: 'json'}).pipe(
      map(response => response.data),
      retryWhen(errors => errors.pipe(delay(1000), take(100))),
    ).subscribe(config => this.openidConfig = config);
  }

  base64URLEncode(str) {
    return str.toString('base64')
      .replace(/\+/g, '-')
      .replace(/\//g, '_')
      .replace(/=/g, '');
  }

  sha256(buffer) {
    const bitArray = sjcl.hash.sha256.hash(buffer);
    return sjcl.codec.hex.fromBits(bitArray);
  }

  @Get('auth/login')
  async login(@Req() req: Request, @Res() res: Response) {
    this.logger.debug(CLIENT_ID);
    if (req.query.code) {
      const q = req.query as CodeResponse;

      this.http.post(
        encodeURI(
          this.openidConfig.token_endpoint
          + [
            '?grant_type=authorization_code',
            `&client_id=${CLIENT_ID}`,
            `&client_secret=${CLIENT_SECRET}`,
            `&code=${q.code}`,
            `&redirect_uri=${req.protocol}://${req.get('Host')}/auth/login`,
          ].join(''),
        ),
        {
          // grant_type: 'authorization_code',
          // client_id: CLIENT_ID,
          // client_secret: CLIENT_SECRET,
          // code: q.code,
          // redirect_uri: `${req.protocol}://${req.get('Host')}/auth/login`,
        },
        {
          // headers: {authorization: 'Basic ' + new Buffer(`${CLIENT_ID}:${CLIENT_SECRET}`).toString('base64') },
          headers: {
            'content-type': 'application/x-www-form-urlencoded',
          },
          responseType: 'json',
        })
        .pipe(
          map(response => response.data),
        ).subscribe(
        async (response: TokenResponse) => {
          this.logger.debug(response);
          const decoded = this.jwtService.decode(response.id_token) as IdToken;
          this.logger.debug(decoded);
          let user = await this.usersService.findUserByIdentity(decoded.iss, decoded.sub);

          if (!user || !user._id) {
            this.logger.debug('CREATE NEW USER');
            user = await this.usersService.create({
              identities: [decoded],
            });
          } else {
            this.logger.debug('USER EXISTS.');
            user = await this.usersService.updateUserIdentities(user, decoded);
          }
          this.logger.debug(user);
          // res.status(200).json();
          const token = this.authService.generateToken(user);
          this.logger.debug(token);
          // const token = Buffer.from(JSON.stringify(jwtToken)).toString('base64')
          //   .replace(/=/g, '')
          //   .replace(/\+/g, '')
          //   .replace(/\//g, '_');

          res.redirect('/#access_token=' + encodeURI(token.access_token));
        },
        error => {
          this.logger.error(error);
          res.status(500).end();
        },
      );
    }

    if (!req.query || (req.query && !req.query.code)) {
      // wellKnownOIDCConfiguration = JSON.parse(JSON.stringify(wellKnownOIDCConfiguration));
      // this.logger.debug(wellKnownOIDCConfiguration);
      const authorizationEndpoint = this.openidConfig.authorization_endpoint;
      const responseType = 'code';
      const nonce = _.times(16, () => _.random(25).toString(36)).join('');
      const responseMode = 'query'; // only support response_type=code

      const verifier = this.base64URLEncode(_.times(32, () => _.random(25).toString(36)).join(''));
      const challenge = this.base64URLEncode(this.sha256(verifier));
      // const link = encodeURI((authorizationEndpoint +
      //   `?scope=openid profile email&response_type=${responseType}&client_id=${CLIENT_ID}&code_challenge=${challenge}`
      //   + `&code_challenge_method=S256&nonce=${nonce}&response_mode=${responseMode}&redirect_uri=`).trim());
      const link = encodeURI(
        [
          authorizationEndpoint,
          `?scope=${this.openidConfig.scopes_supported.join(' ')}`,
          `&response_type=code`,
          `&client_id=${CLIENT_ID}`,
          `&response_mode=query`,
          `&redirect_uri=`,
        ].join(''),
      );
      this.logger.debug(link);
      // window.location.href = link;
      // return res.json(link);
      return res.render('login', {link});
    }
    // return res.redirect(303, `http://redirect.com`);
    // return this.authService.login(req.user);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('auth/@me')
  getProfile(@Req() req) {
    return req.user;
  }
}
