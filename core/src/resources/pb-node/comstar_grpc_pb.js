// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var comstar_pb = require('./comstar_pb.js');
var google_protobuf_any_pb = require('google-protobuf/google/protobuf/any_pb.js');
var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');
var google_protobuf_duration_pb = require('google-protobuf/google/protobuf/duration_pb.js');
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');

function serialize_google_protobuf_Empty(arg) {
  if (!(arg instanceof google_protobuf_empty_pb.Empty)) {
    throw new Error('Expected argument of type google.protobuf.Empty');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_google_protobuf_Empty(buffer_arg) {
  return google_protobuf_empty_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_nghinhut_comstar_Event(arg) {
  if (!(arg instanceof comstar_pb.Event)) {
    throw new Error('Expected argument of type nghinhut.comstar.Event');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_nghinhut_comstar_Event(buffer_arg) {
  return comstar_pb.Event.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_nghinhut_comstar_HealthCheckRequest(arg) {
  if (!(arg instanceof comstar_pb.HealthCheckRequest)) {
    throw new Error('Expected argument of type nghinhut.comstar.HealthCheckRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_nghinhut_comstar_HealthCheckRequest(buffer_arg) {
  return comstar_pb.HealthCheckRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_nghinhut_comstar_HealthCheckResponse(arg) {
  if (!(arg instanceof comstar_pb.HealthCheckResponse)) {
    throw new Error('Expected argument of type nghinhut.comstar.HealthCheckResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_nghinhut_comstar_HealthCheckResponse(buffer_arg) {
  return comstar_pb.HealthCheckResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_nghinhut_comstar_SubscriptionRequest(arg) {
  if (!(arg instanceof comstar_pb.SubscriptionRequest)) {
    throw new Error('Expected argument of type nghinhut.comstar.SubscriptionRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_nghinhut_comstar_SubscriptionRequest(buffer_arg) {
  return comstar_pb.SubscriptionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


var ComstarService = exports.ComstarService = {
  //  rpc AddJob(Job) returns (google.protobuf.Empty) {}
  //  rpc ListJobsStream (google.protobuf.Empty) returns (stream Job) {}
  //
  // for runner
  //  rpc StreamJobStream (stream Job) returns (stream Job) {}
  //
  // Event RPCs
  streamEventsStream: {
    path: '/nghinhut.comstar.Comstar/StreamEventsStream',
    requestStream: true,
    responseStream: true,
    requestType: comstar_pb.Event,
    responseType: comstar_pb.Event,
    requestSerialize: serialize_nghinhut_comstar_Event,
    requestDeserialize: deserialize_nghinhut_comstar_Event,
    responseSerialize: serialize_nghinhut_comstar_Event,
    responseDeserialize: deserialize_nghinhut_comstar_Event,
  },
  subscription: {
    path: '/nghinhut.comstar.Comstar/Subscription',
    requestStream: false,
    responseStream: true,
    requestType: comstar_pb.SubscriptionRequest,
    responseType: comstar_pb.Event,
    requestSerialize: serialize_nghinhut_comstar_SubscriptionRequest,
    requestDeserialize: deserialize_nghinhut_comstar_SubscriptionRequest,
    responseSerialize: serialize_nghinhut_comstar_Event,
    responseDeserialize: deserialize_nghinhut_comstar_Event,
  },
  //  rpc Subscription(google.protobuf.Empty) returns (stream Event) {}
  publishAnEvent: {
    path: '/nghinhut.comstar.Comstar/PublishAnEvent',
    requestStream: false,
    responseStream: false,
    requestType: comstar_pb.Event,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_nghinhut_comstar_Event,
    requestDeserialize: deserialize_nghinhut_comstar_Event,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
};

exports.ComstarClient = grpc.makeGenericClientConstructor(ComstarService);
//  rpc ListUsersStream (ListUsersRequest) returns (stream User) { option (google.api.http) = { get: "/users" }; }
//  rpc GetUserById (GetUserByIdRequest) returns (User) { option (google.api.http) = { get: "/users" }; }
var HealthService = exports.HealthService = {
  check: {
    path: '/nghinhut.comstar.Health/Check',
    requestStream: false,
    responseStream: false,
    requestType: comstar_pb.HealthCheckRequest,
    responseType: comstar_pb.HealthCheckResponse,
    requestSerialize: serialize_nghinhut_comstar_HealthCheckRequest,
    requestDeserialize: deserialize_nghinhut_comstar_HealthCheckRequest,
    responseSerialize: serialize_nghinhut_comstar_HealthCheckResponse,
    responseDeserialize: deserialize_nghinhut_comstar_HealthCheckResponse,
  },
  watch: {
    path: '/nghinhut.comstar.Health/Watch',
    requestStream: false,
    responseStream: true,
    requestType: comstar_pb.HealthCheckRequest,
    responseType: comstar_pb.HealthCheckResponse,
    requestSerialize: serialize_nghinhut_comstar_HealthCheckRequest,
    requestDeserialize: deserialize_nghinhut_comstar_HealthCheckRequest,
    responseSerialize: serialize_nghinhut_comstar_HealthCheckResponse,
    responseDeserialize: deserialize_nghinhut_comstar_HealthCheckResponse,
  },
};

exports.HealthClient = grpc.makeGenericClientConstructor(HealthService);
