// GENERATED CODE -- DO NOT EDIT!

// package: nghinhut.comstar
// file: comstar.proto

import * as comstar_pb from "./comstar_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as grpc from "grpc";

interface IComstarService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
  streamEventsStream: grpc.MethodDefinition<comstar_pb.Event, comstar_pb.Event>;
  subscription: grpc.MethodDefinition<comstar_pb.SubscriptionRequest, comstar_pb.Event>;
  publishAnEvent: grpc.MethodDefinition<comstar_pb.Event, google_protobuf_empty_pb.Empty>;
}

export const ComstarService: IComstarService;

export class ComstarClient extends grpc.Client {
  constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
  streamEventsStream(metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientDuplexStream<comstar_pb.Event, comstar_pb.Event>;
  streamEventsStream(metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientDuplexStream<comstar_pb.Event, comstar_pb.Event>;
  subscription(argument: comstar_pb.SubscriptionRequest, metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientReadableStream<comstar_pb.Event>;
  subscription(argument: comstar_pb.SubscriptionRequest, metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientReadableStream<comstar_pb.Event>;
  publishAnEvent(argument: comstar_pb.Event, callback: grpc.requestCallback<google_protobuf_empty_pb.Empty>): grpc.ClientUnaryCall;
  publishAnEvent(argument: comstar_pb.Event, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<google_protobuf_empty_pb.Empty>): grpc.ClientUnaryCall;
  publishAnEvent(argument: comstar_pb.Event, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<google_protobuf_empty_pb.Empty>): grpc.ClientUnaryCall;
}

interface IHealthService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
  check: grpc.MethodDefinition<comstar_pb.HealthCheckRequest, comstar_pb.HealthCheckResponse>;
  watch: grpc.MethodDefinition<comstar_pb.HealthCheckRequest, comstar_pb.HealthCheckResponse>;
}

export const HealthService: IHealthService;

export class HealthClient extends grpc.Client {
  constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
  check(argument: comstar_pb.HealthCheckRequest, callback: grpc.requestCallback<comstar_pb.HealthCheckResponse>): grpc.ClientUnaryCall;
  check(argument: comstar_pb.HealthCheckRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<comstar_pb.HealthCheckResponse>): grpc.ClientUnaryCall;
  check(argument: comstar_pb.HealthCheckRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<comstar_pb.HealthCheckResponse>): grpc.ClientUnaryCall;
  watch(argument: comstar_pb.HealthCheckRequest, metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientReadableStream<comstar_pb.HealthCheckResponse>;
  watch(argument: comstar_pb.HealthCheckRequest, metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientReadableStream<comstar_pb.HealthCheckResponse>;
}
