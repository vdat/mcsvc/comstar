import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  identities: [
    {
      _id: false,
      iss: String,
      sub: String,
      picture: String,
    },
  ],
});
