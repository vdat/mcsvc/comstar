import {Inject, Injectable, Logger} from '@nestjs/common';
import {User} from './interfaces/user.interface';
import {CreateUserDto} from './dto/create-job.dto';
import { Model } from 'mongoose';
import {IdToken} from './interfaces/id-token.interface';
import * as _ from 'lodash';

@Injectable()
export class UsersService {
  private readonly logger = new Logger(this.constructor.name);

  constructor(@Inject('USER_MODEL') private readonly userModel: Model<User>) {}

  async findUserByIdentity(iss: string, sub: string) {
    let result;
    try {
      const query = this.userModel.findOne(
        {
          'identities.iss': iss,
          'identities.sub': sub,
        },
      );
      const promise = query.exec();
      result = await promise;
    } catch (error) {
      this.logger.error(error);
    }
    return result;
  }

  async create(createDto: CreateUserDto): Promise<User> {
    const created = new this.userModel(createDto);
    return await created.save();
  }

  // async findOne(username: string): Promise<User | undefined> {
  //   return this.users.find(user => user.username === username);
  // }
  async updateUserIdentities(user, decoded: IdToken) {
    for (let i = 0; i < user.identities.length; i++) {
      const idToken = user.identities[i];
      if (idToken.iss === decoded.iss && idToken.sub === decoded.sub) {
        user.identities[i] = decoded;
      }
    }
    this.logger.debug(user);

    return await user.save();
  }
}
