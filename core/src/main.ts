import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {join} from 'path';
import {Transport} from '@nestjs/common/enums/transport.enum';
import {NestExpressApplication} from '@nestjs/platform-express';

async function bootstrap() {
  // const app = await NestFactory.create(AppModule);
  // await app.listen(3000);

  // const app = await NestFactory.createMicroservice(AppModule, {
  //   transport: Transport.GRPC,
  //   options: {
  //     package: 'comstar.v1',
  //     protoPath: join(__dirname, '../../comstar.proto'),
  //     loader: {
  //       includeDirs: [
  //         join(__dirname, 'scripts/protoc/include/'),
  //         join(process.env.GOPATH, '/src/github.com/googleapis/googleapis/'),
  //       ],
  //     },
  //   },
  // });
  // await app.listenAsync();

  // const app = await NestFactory.create(AppModule);
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.setBaseViewsDir(join(__dirname, 'views'));
  app.setViewEngine('ejs');
  // gRPC
  app.connectMicroservice({
    transport: Transport.GRPC,
    options: {
      url: '0.0.0.0:5000',
      package: 'nghinhut.comstar',
      protoPath: join(__dirname, 'comstar.proto'),
      loader: { includeDirs: [join(__dirname)] },
    },
  });
  // HTTP
  app.connectMicroservice({
    transport: Transport.TCP,
    options: { retryAttempts: 5, retryDelay: 3000},
  });
  await app.startAllMicroservicesAsync();
  await app.listen(3001);
}
bootstrap();
