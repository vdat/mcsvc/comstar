import * as Queue from 'bull';

const jobQueue = new Queue('job queue');
// const jobQueue = new Queue('job queue', 'redis://redis:6379');

jobQueue.process((job, done) => {

});

const addJob = job => {
  jobQueue.add(job);
};

export { addJob };
