import {Controller, Get, Logger, OnModuleInit} from '@nestjs/common';
import {Client, ClientGrpc, GrpcMethod, GrpcStreamCall, GrpcStreamMethod} from '@nestjs/microservices';
import {Observable, Subject} from 'rxjs';
import {Stream} from 'stream';
import * as moment from 'moment';

@Controller()
export class Comstar {
  private readonly logger = new Logger(this.constructor.name);

  // @GrpcStreamMethod()
  // async streamJobStream(messages: Observable<any>): Promise<any> {
  //   const s = new Subject();
  //   const o = s.asObservable();
  //   setInterval(() => {
  //     s.next({
  //       recipientNumber: '09xxxxxx',
  //       message: 'sent message! at ' + moment() ,
  //     });
  //   }, 500);
  //
  //   messages.subscribe(msg => {
  //     this.logger.log(msg);
  //   });
  //   return o;
  // }
  // @GrpcMethod()
  // GetUserById() {
  //  return;
  // }
}
