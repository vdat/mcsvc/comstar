import {HttpModule, Module} from '@nestjs/common';
import { ComstarModule } from './comstar/comstar.module';
import { HealthModule } from './health/health.module';
import {JobsModule} from './jobs/jobs.module';
import {JwtModule} from '@nestjs/jwt';
import {Health} from './health/health.controller';
import {AuthController} from './auth/auth.controller';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import {join} from 'path';
import {AuthService} from './auth/auth.service';
import {EventsModule} from './events/events.module';

@Module({
  imports: [
    HealthModule,
    JwtModule.register({ secret: process.env.SECRET || 'hard!to-guess_secret' }),
    AuthModule,
    HttpModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    EventsModule,
    JobsModule,
    UsersModule,
    ComstarModule,
  ],
  controllers: [Health, AuthController],
  providers: [],
})
export class AppModule {}
