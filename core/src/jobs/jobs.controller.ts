import {Controller, Get, Post, Body, Logger} from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { JobsService } from './jobs.service';
import { Job } from './interfaces/job.interface';
import {GrpcMethod, GrpcStreamMethod} from '@nestjs/microservices';
import {Observable, Subject} from 'rxjs';

// @Controller('jobs')
// export class JobController {
//   constructor(private readonly jobsService: JobsService) {}
//
//   @Post()
//   async create(@Body() createJobDto: CreateJobDto) {
//     this.jobsService.create(createJobDto);
//   }
//
//   @Get()
//   async findAll(): Promise<Job[]> {
//     return this.jobsService.findAll();
//   }
// }

@Controller()
export class JobController {
  private readonly logger = new Logger(this.constructor.name);
  constructor(private readonly jobsService: JobsService) {}

  @GrpcStreamMethod('Comstar', 'StreamJobStream')
  async streamJobStream(messages: Observable<any>): Promise<any> {
    const s = new Subject();
    const o = s.asObservable();

    messages.subscribe(msg => {
      this.logger.log(msg);
    });
    return o;
  }
}
