import { Module } from '@nestjs/common';
import {JobController} from './jobs.controller';
import { JobsService } from './jobs.service';
import { jobsProviders } from './jobs.provider';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [JobController],
  providers: [JobsService, ...jobsProviders],
})
export class JobsModule {}
