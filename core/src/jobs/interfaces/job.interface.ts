import { Document } from 'mongoose';

export interface Job extends Document {
  readonly name: string;
  readonly age: number;
  readonly breed: string;
}
