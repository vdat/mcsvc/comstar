import * as mongoose from 'mongoose';

export const JobSchema = new mongoose.Schema({
  name: String,
  age: Number,
  breed: String,
});
