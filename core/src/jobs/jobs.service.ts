import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateJobDto } from './dto/create-job.dto';
import { Job } from './interfaces/job.interface';

@Injectable()
export class JobsService {
  constructor(@Inject('JOB_MODEL') private readonly jobModel: Model<Job>) {}

  async create(createJobDto: CreateJobDto): Promise<Job> {
    const createdJob = new this.jobModel(createJobDto);
    return await createdJob.save();
  }

  async findAll(): Promise<Job[]> {
    return await this.jobModel.find().exec();
  }


}
