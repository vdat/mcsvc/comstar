import * as shell from 'shelljs';

shell.cp('-R', 'src/*.proto', 'dist/');
shell.cp('-R', 'src/include/', 'dist/include/');
shell.cp('-R', 'src/resources/', 'dist/resources/');
