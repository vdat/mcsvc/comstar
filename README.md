# Comstar
Android SMS Gateway

## Directory
    .
    ├── app                     # Android application 
    ├── core                    # Core service (NestJS)
    ├── dashboard               # Angular web app provice UI
    ├── etc                     # Scripts, docker images, etc.
    ├── comstar.proto           # Proto3 file (there're copies in subdirectory to generate code setup)
    ├── docker-compose.yml      # Quickstart Docker Compose file
    ├── Dockerfile              # Core component Docker image
    ├── LICENSE
    └── README.md

## Use cases
- Your application need to send SMS to user, or also receive reply.
- Your provider does not provide API to send SMS back to your system.

## Changes needed: 
1. You need to add a new function on your current system to call send SMS API.
1. Your system must provide a webhook in order to get SMS received by your phone

## Requirements:
1. Android 6.0 or later phone

## Built With
* [gRPC](https://grpc.io) A high performance, open-source universal RPC framework (used by all component to communicate)
* [NestJS](https://nestjs.com) A progressive Node.js framework for building efficient, reliable and scalable server-side applications. (Core Service)
* [Angular](https://angular.io) A TypeScript-based open-source web application framework (Dashboard with PWA & SSR enabled)
* [Docker]() Containerize core component
* [Helm](https://helm.sh/) Helm chart to deploy on Kubernetes Cluster
* [Android]() Mobile Application
* [GitLab CI]() for CI/CD
* [Black Dashboard Angular](https://www.creative-tim.com/product/black-dashboard-angular) Free Bootstrap 4 & Angular 8 Admin Template
