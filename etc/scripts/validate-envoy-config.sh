
docker run --rm -v "$PWD/core/envoy":/etc/envoy -it envoyproxy/envoy:v1.11.1 \
  sh -c "/usr/local/bin/envoy --config-yaml /etc/envoy/envoy.yaml --mode validate"
