PROTOC=etc/scripts/protoc/bin/protoc
INCLUDE=protos
OUT_DIR=gen/pb-web/
PROTOC_GEN_GRPC_WEB=etc/scripts/protoc-gen-grpc-web-1.0.6-linux-x86_64


#go get -u github.com/googleapis/googleapis/google/api

mkdir -p $OUT_DIR


$PROTOC -I. -I=$INCLUDE comstar.proto \
--plugin=protoc-gen-grpc-web=$PROTOC_GEN_GRPC_WEB \
--js_out=import_style=commonjs:$OUT_DIR \
--grpc-web_out=import_style=commonjs+dts,mode=grpcwebtext:$OUT_DIR

$PROTOC -I. -I=$INCLUDE google/api/annotations.proto \
--plugin=protoc-gen-grpc-web=$PROTOC_GEN_GRPC_WEB \
--js_out=import_style=commonjs:$OUT_DIR \
--grpc-web_out=import_style=commonjs,mode=grpcwebtext:$OUT_DIR

$PROTOC -I. -I=$INCLUDE google/api/http.proto \
--plugin=protoc-gen-grpc-web=$PROTOC_GEN_GRPC_WEB \
--js_out=import_style=commonjs:$OUT_DIR \
--grpc-web_out=import_style=commonjs,mode=grpcwebtext:$OUT_DIR

cp -R gen/pb-web dashboard/src/resources/
