
export COMSTAR_PROTO_FILE=comstar.proto

#docker run -v "$PWD":/defs namely/protoc-all -f $COMSTAR_PROTO_FILE -l java
docker run -v "$PWD":/defs namely/protoc-all -i protos -f $COMSTAR_PROTO_FILE -l node --with-typescript
docker run -v "$PWD":/defs namely/protoc-all -i protos -f google/api/annotations.proto -l node --with-typescript
docker run -v "$PWD":/defs namely/protoc-all -i protos -f google/api/http.proto -l node --with-typescript
#docker run -v "$PWD":/defs namely/gen-grpc-gateway -f protos/$COMSTAR_PROTO_FILE -s Comstar
#./etc/scripts/protogen-grpc-web.sh

sudo chown -R master gen/

cp protos/comstar.proto core/src/comstar.proto
cp protos/comstar.proto app/app/src/main/proto/comstar.proto

#rm -rf core/grpc-gateway && mv gen/grpc-gateway/ core/
rm -rf core/src/resources/pb-node/ && mv gen/pb-node/ core/src/resources/
#rm -rf gen/

etc/scripts/grpc-web-protogen.sh
